package com.waracle.interviewtest.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.waracle.interviewtest.R;
import com.waracle.interviewtest.cakeRecycler.CakesAdapter;
import com.waracle.interviewtest.model.Cake;
import com.waracle.interviewtest.net.DataLoader;

import java.util.List;

public class CakesListFragment extends Fragment
{
	private static final String TAG = CakesListFragment.class.getSimpleName();

	private static String JSON_URL = "https://gist.githubusercontent.com/hart88/198f29ec5114a3ec3460/" + "raw/8dd19a88f9b8d24c23d9960f3300d0c917a4f07c/cake.json";

	private ListView mListView;
	private CakesAdapter mAdapter;

	public CakesListFragment() { }

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View rootView = inflater.inflate(R.layout.fragment_main, container, false);
		mListView = (ListView) rootView.findViewById(R.id.list);
		return rootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState)
	{
		super.onActivityCreated(savedInstanceState);

		// Create and set the list adapter.
		mAdapter = new CakesAdapter(getContext());
		mListView.setAdapter(mAdapter);

		reloadData();
	}

	public void reloadData(){
		DataLoader.getCakes(JSON_URL, new DataLoader.CakesListener()
		{
			@Override
			public void onCakesDownloaded(List<Cake> cakeList)
			{
				mAdapter.setItems(cakeList);
				mAdapter.notifyDataSetChanged();
				Log.d(TAG, "notifyt data: " + cakeList.toString());
			}
		});
	}


}
