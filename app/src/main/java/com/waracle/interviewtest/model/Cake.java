package com.waracle.interviewtest.model;

import org.json.JSONException;
import org.json.JSONObject;

public class Cake
{

	private String title;
	private String desc;
	private String image;

	/**
	 *
	 * @return
	 * The title
	 */
	public String getTitle()
	{
		return title;
	}

	/**
	 *
	 * @param title
	 * The title
	 */
	public void setTitle(String title)
	{
		this.title = title;
	}

	/**
	 *
	 * @return
	 * The desc
	 */
	public String getDesc()
	{
		return desc;
	}

	/**
	 *
	 * @param desc
	 * The desc
	 */
	public void setDesc(String desc)
	{
		this.desc = desc;
	}

	/**
	 *
	 * @return
	 * The image
	 */
	public String getImage()
	{
		return image;
	}

	/**
	 *
	 * @param image
	 * The image
	 */
	public void setImage(String image)
	{
		this.image = image;
	}

	public static Cake fromJson(JSONObject jsonObject){
		if (jsonObject == null){
			return null;
		}
		Cake cake = new Cake();
		try
		{
			cake.setDesc(jsonObject.getString("desc"));
			cake.setImage(jsonObject.getString("image"));
			cake.setTitle(jsonObject.getString("title"));
		}
		catch (JSONException e)
		{
			e.printStackTrace();
		}
		return cake;
	}

}
