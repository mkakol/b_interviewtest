package com.waracle.interviewtest.net;

import android.os.AsyncTask;

import com.waracle.interviewtest.model.Cake;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class DataLoader
{
	public interface CakesListener {
		public void onCakesDownloaded(List<Cake> cakeList);
	}

	public static void getCakes(String siteUrl, CakesListener cakesListener){
		// Here is ideal place for RxJava obserwables but lets use AsyncTask (no external library rule)
		new CakesAsyncTask(cakesListener).execute(siteUrl);
	}

	private static JSONArray loadData(String siteUrl) throws IOException, JSONException
	{
		URL url = new URL(siteUrl);
		HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
		try
		{
			InputStream in = new BufferedInputStream(urlConnection.getInputStream());

			// Can you think of a way to improve the performance of loading data
			// using HTTP headers???

			// Also, Do you trust any utils thrown your way????

			byte[] bytes = StreamUtils.readUnknownFully(in);

			// Read in charset of HTTP content.
			String charset = parseCharset(urlConnection.getRequestProperty("Content-Type"));

			// Convert byte array to appropriate encoded string.
			String jsonText = new String(bytes, charset);

			// Read string as JSON.
			return new JSONArray(jsonText);
		}
		finally
		{
			urlConnection.disconnect();
		}
	}

	/**
	 * Returns the charset specified in the Content-Type of this header,
	 * or the HTTP default (ISO-8859-1) if none can be found.
	 */
	public static String parseCharset(String contentType)
	{
		if (contentType != null)
		{
			String[] params = contentType.split(",");
			for (int i = 1; i < params.length; i++)
			{
				String[] pair = params[i].trim().split("=");
				if (pair.length == 2)
				{
					if (pair[0].equals("charset"))
					{
						return pair[1];
					}
				}
			}
		}
		return "UTF-8";
	}

	public static class CakesAsyncTask extends AsyncTask<String, Void, List<Cake>> {

		CakesListener mCakesListener;

		CakesAsyncTask(CakesListener cakesListener){
			mCakesListener = cakesListener;
		}

		@Override
		protected List<Cake> doInBackground(String... params)
		{
			String url = params[0];
			List<Cake> cakeList = new ArrayList<>();
			try
			{
				// here we should use either Gson/Jackson to go from JSON to model, or use Retrofit to do it more complex
				JSONArray jsonArray = DataLoader.loadData(url);
				if (jsonArray != null){
					for (int i = 0; i < jsonArray.length(); i++){
						cakeList.add(Cake.fromJson(jsonArray.getJSONObject(i)));
					}
				}
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
			catch (JSONException e)
			{
				e.printStackTrace();
			}
			return cakeList;
		}

		@Override
		protected void onPostExecute(List<Cake> cakeList)
		{
			super.onPostExecute(cakeList);
			if (mCakesListener != null){
				mCakesListener.onCakesDownloaded(cakeList);
			}
		}
	}

}
