package com.waracle.interviewtest.imageLoader;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;
import android.widget.ImageView;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.InvalidParameterException;
import java.util.HashMap;
import java.util.Map;


public class ImageLoader
{

	private static final int MAX_IMAGE_SIZE = 100;

	Map<ImageView, ImageLoaderAsync> mAsyncMap = new HashMap<>();

	private static final String TAG = ImageLoader.class.getSimpleName();

	public interface ImageLoadedListener
	{
		public void onImageLoaded(Bitmap bitmap);
	}

	public ImageLoader() { /**/ }

	/**
	 * Simple function for loading a bitmap image from the web
	 *
	 * @param url       image url
	 * @param imageView view to set image too.
	 */
	public void load(final String url, final ImageView imageView)
	{

		imageView.setImageBitmap(null);
		imageView.setTag(null);

		if (TextUtils.isEmpty(url))
		{
			throw new InvalidParameterException("URL is empty!");
		}

		ImageLoaderAsync imageLoaderAsync = mAsyncMap.get(imageView);
		if (imageLoaderAsync != null)
		{
			imageLoaderAsync.cancel(true);
		}

		RecyclingBitmapDrawable recyclingBitmapDrawable = LruCacheManager.getInstance().getBitmapFromMemCache(url);
		if (recyclingBitmapDrawable != null)
		{
			imageView.setImageDrawable(recyclingBitmapDrawable);
		}


		// Can you think of a way to improve loading of bitmaps
		// that have already been loaded previously??
		ImageLoadedListener imageLoadedListener = new ImageLoadedListener()
		{
			@Override
			public void onImageLoaded(Bitmap bitmap)
			{
				if (imageView.getTag() != null && imageView.getTag() == this)
				{
					LruCacheManager.getInstance().addBitmapToMemoryCache(url, new RecyclingBitmapDrawable(null, bitmap));
					imageView.setImageBitmap(bitmap);
				}
			}
		};
		imageView.setTag(imageLoadedListener);
		imageLoaderAsync = new ImageLoaderAsync(imageLoadedListener);
		mAsyncMap.put(imageView, imageLoaderAsync);

		imageLoaderAsync.execute(url);
	}

	private static Bitmap getBitmapFromUrl(String url) throws IOException
	{
		Bitmap bitmap = null;

		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = false;
		float sampleSizeF = (float) MAX_IMAGE_SIZE / (float) MAX_IMAGE_SIZE;
		int sampleSize = Math.round(sampleSizeF);
		options.inSampleSize = sampleSize;
		try
		{
			bitmap = BitmapFactory.decodeStream((InputStream) new URL(url).getContent(), null, options);
		}
		catch (MalformedURLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return bitmap;
	}

	class ImageLoaderAsync extends AsyncTask<String, Void, Bitmap>
	{
		ImageLoadedListener mImageLoadedListener;

		ImageLoaderAsync(ImageLoadedListener imageLoadedListener)
		{
			mImageLoadedListener = imageLoadedListener;
		}

		@Override
		protected Bitmap doInBackground(String... params)
		{
			String url = params[0];
			try
			{
				return getBitmapFromUrl(url);
			}
			catch (IOException e)
			{
				Log.e(TAG, e.getMessage());
			}
			return null;
		}

		@Override
		protected void onPostExecute(Bitmap bitmap)
		{
			super.onPostExecute(bitmap);
			if (bitmap != null && mImageLoadedListener != null)
			{
				mImageLoadedListener.onImageLoaded(bitmap);
			}
		}
	}
}
