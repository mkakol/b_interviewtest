package com.waracle.interviewtest.cakeRecycler;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.waracle.interviewtest.imageLoader.ImageLoader;
import com.waracle.interviewtest.R;
import com.waracle.interviewtest.model.Cake;

import java.util.List;

public class CakesAdapter extends BaseAdapter
{
	private List<Cake> mItems;
	private ImageLoader mImageLoader;
	LayoutInflater mLayoutInflater;

	public CakesAdapter(Context context)
	{
		mLayoutInflater = LayoutInflater.from(context);
		mImageLoader = new ImageLoader();
	}

	@Override
	public int getCount()
	{
		return mItems == null ? 0 : mItems.size();
	}

	@Override
	public Cake getItem(int position)
	{
		return mItems == null ? null : mItems.get(position);
	}

	@Override
	public long getItemId(int position)
	{
		return 0;
	}

	@SuppressLint("ViewHolder")
	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		ViewHolder viewHolder;
		if (convertView == null)
		{
			convertView = mLayoutInflater.inflate(R.layout.list_item_layout, parent, false);
			viewHolder = new ViewHolder();
			viewHolder.title = (TextView) convertView.findViewById(R.id.title);
			viewHolder.descritpion = (TextView) convertView.findViewById(R.id.desc);
			viewHolder.image = (ImageView) convertView.findViewById(R.id.image);
		}
		else
		{
			viewHolder = (ViewHolder) convertView.getTag();
		}

		Cake cake = getItem(position);

		viewHolder.descritpion.setText(cake.getDesc());
		viewHolder.title.setText(cake.getTitle());

		// changed a little bit, but I would use (or copy) working solution, like UniversalImageLoader, Picasso or any other third party library that's doing it right
		mImageLoader.load(cake.getImage(), viewHolder.image);

		convertView.setTag(viewHolder);

		return convertView;
	}

	public void setItems(List<Cake> items)
	{
		mItems = items;
	}

	class ViewHolder
	{
		TextView title;
		TextView descritpion;
		ImageView image;
	}
}

